<?php

namespace SpringGDSTest\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\Router;

/**
 * Class SpringGDSTestRouteServiceProvider
 * @package SpringGDSTest\Providers
 */
class SpringGDSTestRouteServiceProvider extends RouteServiceProvider
{
    /**
     * @param Router $router
     */
    public function map(Router $router): void
    {
    }

}
