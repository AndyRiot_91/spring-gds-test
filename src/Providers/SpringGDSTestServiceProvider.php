<?php

namespace SpringGDSTest\Providers;

use Plenty\Modules\Order\Shipping\ServiceProvider\Services\ShippingServiceProviderService;
use Plenty\Plugin\ServiceProvider;

/**
 * Class SpringGDSTestServiceProvider
 * @package SpringGDSTest\Providers
 */
class SpringGDSTestServiceProvider extends ServiceProvider
{
    /** Register the service provider. */
    public function register(): void
    {
        // add REST routes by registering a RouteServiceProvider if necessary
//        $this->getApplication()->register(SpringGDSTestRouteServiceProvider::class);
    }

    /** @param ShippingServiceProviderService $shippingServiceProviderService */
    public function boot(ShippingServiceProviderService $shippingServiceProviderService): void
    {
        $shippingServiceProviderService->registerShippingProvider(
            'SpringGDSTest',
            [
                'de' => 'SpringGDSTest',
                'en' => 'SpringGDSTest'
            ],
            [
                'SpringGDSTest\\Controllers\\SpringGDSTestController@registerShipments',
                'SpringGDSTest\\Controllers\\SpringGDSTestController@getLabels',
                'SpringGDSTest\\Controllers\\SpringGDSTestController@deleteShipments',
            ]
        );
    }

}
