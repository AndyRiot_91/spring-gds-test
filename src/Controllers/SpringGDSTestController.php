<?php

namespace SpringGDSTest\Controllers;

use Plenty\Modules\Account\Address\Contracts\AddressRepositoryContract;
use Plenty\Modules\Cloud\Storage\Models\StorageObject;
use Plenty\Modules\Order\Contracts\OrderRepositoryContract;
use Plenty\Modules\Order\Shipping\Countries\Contracts\CountryRepositoryContract;
use Plenty\Modules\Order\Shipping\Information\Contracts\ShippingInformationRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Contracts\OrderShippingPackageRepositoryContract;
use Plenty\Modules\Order\Shipping\PackageType\Contracts\ShippingPackageTypeRepositoryContract;
use Plenty\Modules\Order\Shipping\ServiceProvider\Contracts\ShippingServiceProviderRepositoryContract;
use Plenty\Modules\Plugin\Storage\Contracts\StorageRepositoryContract;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\ConfigRepository;
use Plenty\Modules\Order\Shipping\Package\Models\OrderShippingPackage;
use Plenty\Modules\Item\Item\Contracts\ItemRepositoryContract;
use Plenty\Modules\Item\Variation\Contracts\VariationRepositoryContract;
use Plenty\Modules\Item\VariationSku\Contracts\VariationSkuRepositoryContract;

use Plenty\Plugin\Log\Loggable;

/**
 * Class SpringGDSTestController
 * @package SpringGDSTest\Controllers
 */
class SpringGDSTestController extends Controller
{
    use Loggable;

    /** @var array $services */
    private $services = array(
        'TRCK' => 'SpringGDSTest TRACKED',
    );

    /** @var Request $request */
    private $request;

    /** @var OrderRepositoryContract $orderRepository */
    private $orderRepository;

    /** @var AddressRepositoryContract $addressRepository */
    private $addressRepository;

    /** @var OrderShippingPackageRepositoryContract $orderShippingPackage */
    private $orderShippingPackage;

    /** @var ShippingInformationRepositoryContract $shippingInformationRepositoryContract */
    private $shippingInformationRepositoryContract;

    /** @var StorageRepositoryContract $storageRepository */
    private $storageRepository;

    /** @var ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract */
    private $shippingPackageTypeRepositoryContract;

    /** @var  array $createOrderResult */
    private $createOrderResult = [];

    /** @var ConfigRepository $config */
    private $config;

    /** @var ItemRepositoryContract $itemRepositoryContract */
    private $itemRepositoryContract;

    /** @var VariationRepositoryContract $variationRepositoryContract */
    private $variationRepositoryContract;

    /** @var VariationSkuRepositoryContract $variationSkuRepositoryContract */
    private $variationSkuRepositoryContract;

    /** @var CountryRepositoryContract $countryRepositoryContract */
    private $countryRepositoryContract;

    /**
     * SpringGDSTestController constructor.
     * @param Request $request
     * @param OrderRepositoryContract $orderRepository
     * @param AddressRepositoryContract $addressRepositoryContract
     * @param OrderShippingPackageRepositoryContract $orderShippingPackage
     * @param StorageRepositoryContract $storageRepository
     * @param ShippingInformationRepositoryContract $shippingInformationRepositoryContract
     * @param ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract
     * @param ConfigRepository $config
     * @param ItemRepositoryContract $itemRepositoryContract
     * @param VariationRepositoryContract $variationRepositoryContract
     * @param VariationSkuRepositoryContract $variationSkuRepositoryContract
     * @param CountryRepositoryContract $countryRepositoryContract
     * @param ShippingServiceProviderRepositoryContract $shippingServiceProviderRepositoryContract
     */
    public function __construct(Request $request,
                                OrderRepositoryContract $orderRepository,
                                AddressRepositoryContract $addressRepositoryContract,
                                OrderShippingPackageRepositoryContract $orderShippingPackage,
                                StorageRepositoryContract $storageRepository,
                                ShippingInformationRepositoryContract $shippingInformationRepositoryContract,
                                ShippingPackageTypeRepositoryContract $shippingPackageTypeRepositoryContract,
                                ConfigRepository $config,
                                ItemRepositoryContract $itemRepositoryContract,
                                VariationRepositoryContract $variationRepositoryContract,
                                VariationSkuRepositoryContract $variationSkuRepositoryContract,
                                CountryRepositoryContract $countryRepositoryContract,
                                ShippingServiceProviderRepositoryContract $shippingServiceProviderRepositoryContract)
    {
        $this->request = $request;
        $this->orderRepository = $orderRepository;
        $this->addressRepository = $addressRepositoryContract;
        $this->orderShippingPackage = $orderShippingPackage;
        $this->storageRepository = $storageRepository;
        $this->shippingInformationRepositoryContract = $shippingInformationRepositoryContract;
        $this->shippingPackageTypeRepositoryContract = $shippingPackageTypeRepositoryContract;
        $this->config = $config;
        $this->itemRepositoryContract = $itemRepositoryContract;
        $this->variationRepositoryContract = $variationRepositoryContract;
        $this->variationSkuRepositoryContract = $variationSkuRepositoryContract;
        $this->countryRepositoryContract = $countryRepositoryContract;
    }

    /**
     * Registers shipment(s)
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function registerShipments(Request $request, array $orderIds = array()): array
    {
        $shipmentDate = date('Y-m-d');
        $orderIds = $this->getOpenOrderIds(
            $this->getOrderIds($request, $orderIds)
        );

        foreach ($orderIds as $orderId) {
            $service = '';
            $order = $this->orderRepository->findOrderById($orderId);
            $packages = $this->orderShippingPackage->listOrderShippingPackages($order->id);
            $country = $this->countryRepositoryContract->getCountryById($order->deliveryAddress->countryId);

            foreach ($order->orderItems as $orderItem) {
                if ($orderItem->shippingProfileId > 0) {
                    $conf = json_decode($this->config->get('SpringGDSTest.regionRelations', ''), true);
                    !empty($conf) and $service = strtoupper($conf[$country->shippingDestinationId][$orderItem->shippingProfileId]);
                    break;
                }
            }

            $this->dump(array(
                'service' => $service,
                'config' => $this->config->get('SpringGDSTest.regionRelations', '---'),
                'config2' => $this->config->get('SpringGDS.regionRelations', '---'),
                'config3' => $this->config->get('SpringGDSTest.regionRelations.' . $country->shippingDestinationId . '.' . $order->orderItems[0]->shippingProfileId, '---'),
            ));

            if (empty($service)) {
                $service = 'TRCK';
            }

            foreach ($packages as $package) {
                try {
                    $responseApi = $this->curlApi($this->genData($package, $order, $service));

                    // shipping service providers API should be used here
                    $response = [
                        'labelUrl' => $this->genLabelUrl($responseApi->Shipment->TrackingNumber),
                        'shipmentNumber' => $responseApi->Shipment->TrackingNumber,
                        'sequenceNumber' => 1,
                        'status' => 'shipment successfully registered as ' . $this->services[$service],
                    ];

                    // handles the response
                    $shipmentItems = $this->handleAfterRegisterShipment(
                        $response['labelUrl'],
                        $response['shipmentNumber'],
                        $package->id
                    );

                    // adds result
                    $this->createOrderResult[$orderId] = [
                        'success' => (empty($responseApi->Error) && $responseApi->ErrorLevel == 0),
                        'message' => (empty($responseApi->Error) && $responseApi->ErrorLevel == 0
                            ? 'Shipment successfully registered as ' . $this->services[$service]
                            : 'Error Code ' . $responseApi->ErrorLevel . '; API Error: ' . $responseApi->Error),
                        'newPackagenumber' => false,
                        'packages' => $shipmentItems,
                    ];

                    // saves shipping information
                    $this->saveShippingInformation($orderId, $shipmentDate, $shipmentItems);
                } catch (\Exception $e) {
                    // handle exception
                    $this->getLogger('SpringGDSTest')->logException($e);
                }
            }
        }

        return $this->createOrderResult;
    }

    /**
     * Cancels registered shipment(s)
     *
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function deleteShipments(Request $request, $orderIds)
    {
        $orderIds = $this->getOrderIds($request, $orderIds);

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);

            try {
                // use the shipping service provider's API here
                $response = $this->curlApi(
                    json_encode(
                        array(
                            'Apikey' => $this->config->get('SpringGDSTest.apiKey'),
                            'Command' => 'VoidShipment',
                            'Shipment' => array(
                                'TrackingNumber' => $shippingInformation->transactionId,
                            ),
                        )
                    )
                );

                $this->createOrderResult[$orderId] = [
                    'success' => (empty($response->Error) && $response->ErrorLevel == 0),
                    'message' => (empty($response->Error) && $response->ErrorLevel == 0
                        ? 'Shipment canceled'
                        : 'Code: ' . $response->ErrorLevel . ': ' . $response->Error),
                    'newPackagenumber' => false,
                    'packages' => null,
                ];
            } catch (\Exception $e) {
                // exception handling
                $this->getLogger('SpringGDSTest')->logException($e);
            }

            if (isset($shippingInformation->additionalData) && is_array($shippingInformation->additionalData)) {
                // resets the shipping information of current order
                $this->shippingInformationRepositoryContract->resetShippingInformation($orderId);
            }
        }

        // return result array
        return $this->createOrderResult;
    }

    /**
     * @param Request $request
     * @param array $orderIds
     * @return array
     */
    public function getLabels(Request $request, $orderIds = array()): array
    {
        $orderIds = $this->getOrderIds($request, $orderIds);
        $labels = array();

        foreach ($orderIds as $orderId) {
            /** @var OrderShippingPackage $packages */
            $packages = $this->orderShippingPackage->listOrderShippingPackages($orderId);

            foreach ($packages as $package) {
                $labelKey = array_pop(explode('/', $package->labelPath));

                if ($this->storageRepository->doesObjectExist('SpringGDSTest', $labelKey)) {
                    $storageObject = $this->storageRepository->getObject('SpringGDSTest', $labelKey);
                    $labels[] = $storageObject->body;
                }
            }
        }

        return $labels;
    }

    /**
     * @param object $package
     * @param object $order
     * @param string $service
     * @return string
     */
    protected function genData(object $package, object $order, string $service): string
    {
        $items = array();
        $totalPrice = 0.00;
        $packageType = $this->shippingPackageTypeRepositoryContract->findShippingPackageTypeById($package->packageId);

        foreach ($order->orderItems as $item) {
            if ($item->itemVariationId) {
                $varById = (array)$this->variationRepositoryContract->show($item->itemVariationId, array(), 'en');
                $itemById = $this->itemRepositoryContract->show($varById['itemId'], array(), 'en'); // urlPath

                switch ($this->config->get('SpringGDSTest.itemTitle', 'name1')) {
                    case 'name1':
                        $description = $itemById->texts[0]->name1;
                        break;
                    case 'name2':
                        $description = $itemById->texts[0]->name2;
                        break;
                    case 'name3':
                        $description = $itemById->texts[0]->name3;
                        break;
                    case 'shortDescription':
                        $description = $itemById->texts[0]->shortDescription;
                        break;
                    default:
                        $description = $item->orderItemName;
                }

                $rate = (
                count($item->amounts) < count($order->amounts)
                    ? $order->amounts[(int)(!empty($order->amounts[1]) && $order->amounts[1]->currency !== 'EUR')]
                    : $item->amounts[(int)(!empty($item->amounts[1]) && $item->amounts[1]->currency !== 'EUR')]
                )->exchangeRate;

                $price = max(
                    .01,
                    $item->amounts[0]->priceOriginalGross * $item->quantity * ($item->amounts[0]->currency === 'EUR' ? $rate : 1)
                );

                $totalPrice += $price;

                $items[] = array(
                    'Description' => $description,
                    'Sku' => $varById->model ?? $varById['model'],
                    'HsCode' => (!empty($itemById->customsTariffNumber)
                        ? $itemById->customsTariffNumber
                        : $this->config->get('SpringGDSTest.hsCode')
                    ),
                    'OriginCountry' => '',
                    'PurchaseUrl' => '',
                    'Quantity' => $item->quantity,
                    'Value' => number_format($price, 2, '.', ''),
                );
            }
        }

        return json_encode(
            array(
                'Apikey' => $this->config->get('SpringGDSTest.apiKey'),
                "Command" => 'OrderShipment',
                'Shipment' => array(
                    "LabelFormat" => $this->config->get('SpringGDSTest.format', 'PDF'),
                    "DisplayId" => ($this->config->get('SpringGDSTest.displayOrderId', 0)
                        ? $order->id
                        : ''),
                    "Service" => $service,
                    "ConsignorAddress" => array(
                        "Name" => $this->config->get('SpringGDSTest.senderName'),
                        "Company" => $this->config->get('SpringGDSTest.companyName'),
                        "AddressLine1" => $this->config->get('SpringGDSTest.senderStreet') . ' ' . $this->config->get('SpringGDSTest.senderNo'),
                        "AddressLine2" => "",
                        "City" => $this->config->get('SpringGDSTest.senderTown'),
                        "State" => $this->config->get('SpringGDSTest.senderState'),
                        "Zip" => $this->config->get('SpringGDSTest.senderPostalCode'),
                        "Country" => $this->config->get('SpringGDSTest.senderCountryIso'), // iso
                        "Phone" => $this->config->get('SpringGDSTest.senderPhone'),
                        "Email" => $this->config->get('SpringGDSTest.senderEmail'),
                    ),
                    "ConsigneeAddress" => array(
                        "Name" => $order->deliveryAddress->firstName . " " . $order->deliveryAddress->lastName,
                        "Company" => $order->deliveryAddress->companyName,
                        "AddressLine1" => $order->deliveryAddress->address1,
                        "AddressLine2" => $order->deliveryAddress->address2,
                        "City" => $order->deliveryAddress->town,
                        "State" => $order->deliveryAddress->state->isoCode ?: '',
                        "Zip" => $order->deliveryAddress->postalCode,
                        "Country" => $order->deliveryAddress->country->isoCode2,
                        "Phone" => (!empty(trim($order->deliveryAddress->phone)) ? $order->deliveryAddress->phone : '0000000000'),
                        "Email" => $order->deliveryAddress->email,
                        "Vat" => '',
                    ),
                    "Length" => $packageType->length,
                    "Width" => $packageType->width,
                    "Weight" => $package->weight / 1000, // todo
                    "Height" => $packageType->height,
                    "WeightUnit" => "kg", // todo
                    "DimUnit" => "cm", // todo
                    "Value" => number_format($totalPrice, 2, '.', ''),
                    "Currency" => $order->amounts[(int)!empty($order->amounts[1])]->currency,
                    "DeclarationType" => $this->config->get('SpringGDSTest.declarationType'),
                    "Products" => $items,
                    'Source' => 'plentymarkets',
                ),
            )
        );
    }

    /**
     * Retrieves the label file from a given URL and saves it in S3 storage
     *
     * @param $labelUrl
     * @param $key
     * @return StorageObject
     */
    private function saveLabelToS3($labelUrl, $key)
    {
        $ch = curl_init();
        // Set URL to download
        curl_setopt($ch, CURLOPT_URL, $labelUrl);
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // Should cURL return or print out the data? (true = return, false = print)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        // Download the given URL, and return output
        $output = curl_exec($ch);
        // Close the cURL resource, and free system resources
        curl_close($ch);

        return $this->storageRepository->uploadObject('SpringGDSTest', $key, $output);
    }

    /**
     * Saves the shipping information
     *
     * @param int $orderId
     * @param $shipmentDate
     * @param array $shipmentItems
     */
    private function saveShippingInformation(int $orderId, $shipmentDate, array $shipmentItems): void
    {
        $transactionIds = array();

        foreach ($shipmentItems as $shipmentItem) {
            $transactionIds[] = $shipmentItem['shipmentNumber'];
        }

        $shipmentAt = date(\DateTime::W3C, strtotime($shipmentDate));
        $registrationAt = date(\DateTime::W3C);

        $data = [
            'orderId' => $orderId,
            'transactionId' => implode(',', $transactionIds),
            'shippingServiceProvider' => 'SpringGDSTest',
            'shippingStatus' => 'registered',
            'shippingCosts' => 0.00,
            'additionalData' => $shipmentItems,
            'registrationAt' => $registrationAt,
            'shipmentAt' => $shipmentAt
        ];

        $this->shippingInformationRepositoryContract->saveShippingInformation($data);
    }

    /**
     * Returns all order ids with shipping status 'open'
     *
     * @param array $orderIds
     * @return array
     */
    private function getOpenOrderIds(array $orderIds): array
    {
        $openOrderIds = array();

        foreach ($orderIds as $orderId) {
            $shippingInformation = $this->shippingInformationRepositoryContract->getShippingInformationByOrderId($orderId);

            if ($shippingInformation->shippingStatus == null || $shippingInformation->shippingStatus == 'open') {
                $openOrderIds[] = $orderId;
            }
        }

        return $openOrderIds;
    }

    /**
     * Returns all order ids from request object
     *
     * @param Request $request
     * @param $orderIds
     * @return array
     */
    private function getOrderIds(Request $request, $orderIds): array
    {
        if (is_numeric($orderIds)) {
            $orderIds = array($orderIds);
        } elseif (!is_array($orderIds)) {
            $orderIds = $request->get('orderIds');
        }

        return $orderIds;
    }

    /**
     * Handling of response values, fires S3 storage and updates order shipping package
     *
     * @param string $labelUrl
     * @param string $shipmentNumber
     * @param string $sequenceNumber
     * @return array
     */
    private function handleAfterRegisterShipment($labelUrl, $shipmentNumber, $sequenceNumber): array
    {
        $shipmentItems = array();
        $storageObject = $this->saveLabelToS3(
            $labelUrl,
            $shipmentNumber . '.pdf'
        );

        $shipmentItems[] = array(
            'labelUrl' => $labelUrl,
            'shipmentNumber' => $shipmentNumber,
        );

        $this->orderShippingPackage->updateOrderShippingPackage(
            $sequenceNumber,
            [
                'packageNumber' => $shipmentNumber,
                'label' => $storageObject->key
            ]
        );

        return $shipmentItems;
    }

    /**
     * @param string $data
     * @return object
     */
    public function curlApi(string $data): object
    {
        $ch = curl_init(
            'https://mtapi.net/'
            . ($this->config->get('SpringGDSTest.mode', '0') === '0' ? '?testMode=1' : '')
        );
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = json_decode(curl_exec($ch));
        curl_close($ch);

        return $response;
    }

    /**
     * @param string $trackingNumber
     * @return string
     */
    public function genLabelUrl(string $trackingNumber = ''): string
    {
        return 'https://mailingtechnology.com/API/label_plenty.php?testMode='
            . (int)!$this->config->get('SpringGDSTest.mode', '0')
            . '&userId=api&tn=' . $trackingNumber
            . '&label_format=' . $this->config->get('SpringGDSTest.format', 'PDF')
            . '&apikey=' . $this->config->get('SpringGDSTest.apiKey', '');
    }

    private function dump($data)
    {
        $ch = curl_init('https://acpapi.com/dima/plenty/index.php');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query(
                array(
                    'some-paranoia' => 'no',
                    'data' => json_encode($data)
                )
            )
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
    }

}
