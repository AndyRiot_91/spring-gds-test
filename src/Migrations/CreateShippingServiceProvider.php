<?php

namespace SpringGDSTest\Migrations;

use Plenty\Modules\Order\Shipping\ServiceProvider\Contracts\ShippingServiceProviderRepositoryContract;
use Plenty\Plugin\Log\Loggable;

/**
 * Class CreateShippingServiceProviderSpringGDSTest
 * @package SpringGDSTest\Migrations
 */
class CreateShippingServiceProvider
{
    use Loggable;

    /** @var ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository */
    private $shippingServiceProviderRepository;

    /** @param ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository */
    public function __construct(ShippingServiceProviderRepositoryContract $shippingServiceProviderRepository)
    {
        $this->shippingServiceProviderRepository = $shippingServiceProviderRepository;
    }

    /** @return void */
    public function run(): void
    {
        try {
            $this->shippingServiceProviderRepository->saveShippingServiceProvider(
                'SpringGDSTest',
                'SpringGDSTest');
            $this->getLogger('SpringGDSTest')
                ->info('SpringGDSTest shipping service provider successfully added');
        } catch (\Exception $e) {
            $this->getLogger('SpringGDSTest')
                ->critical(
                    'Could not save or update shipping service provider SpringGDSTest. Code ' . $e->getCode()
                    . ': ' . $e->getMessage()
                );
        }
    }

}
