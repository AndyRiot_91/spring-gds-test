# Release Notes for Spring GDS Versand

## v1.0.2 (2020-02-21)
 
 ### Added
- Möglichkeit: Referenz auf dem Etikett
 
### Fixed
- Produktwertfehler für Nicht-EU-Ziele

## v1.0.1 (2020-02-06)
 
### Fixed
- User Guide
 
## v1.0.0 (2020-01-24)
 
### Added
- Ursprüngliche Plugin-Version