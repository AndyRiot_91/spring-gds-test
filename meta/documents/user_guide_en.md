# User Guide Spring GDS Shipping (Versand)
 
This plugin enables the various shipping options of Spring Global Delivery Solutions (GDS) for worldwide shipping of packages and parcels.
Generate shipping labels and receive tracking updates on your shipments. 

## Registering with Spring GDS

In order to use the plugin you need to register with <a href="https://www.spring-gds.com/de/" target="_blank">Spring GDS</a>. An API key will be provided which you can use in setting up the plugin in plentymarkets.

## Setting up Spring GDS Shipping (Versand) in plentymarkets

### Configure the Spring GDS API key

  * Install the Spring GDS plugin under **Plugins » Plugin Overview**.
  * After installation click the Spring GDS plugin and enter the API key provided by Spring GDS under **Configuration » Global**.
  * To generate labels, that you can use for shipping, make sure the plugin is set to **Mode - Productive**. **Mode - Test** is only for testing the IT integration.

### Setup the Shipping Settings

The following steps ensure your plentymarkets environment is set up for working with the Spring GDS Shipping plugin. All settings can be found under **Setup » Orders » Shipping » Settings**.
  * Set up <a href="https://knowledge.plentymarkets.com/en/fulfilment/preparing-the-shipment#100" target="_blank">countries of delivery</a>.
  * Create <a href="https://knowledge.plentymarkets.com/en/fulfilment/preparing-the-shipment#400" target="_blank">regions</a>.
  * Create the <a href="https://knowledge.plentymarkets.com/en/fulfilment/preparing-the-shipment#800" target="_blank">shipping service provider</a> **Spring GDS**.
   Use **Tracking URL**: https://mailingtechnology.com/tracking/?tn=[PaketNr]
  * Create <a href="https://knowledge.plentymarkets.com/en/fulfilment/preparing-the-shipment#1000" target="_blank">shipping profiles</a> and <a href="https://knowledge.plentymarkets.com/en/fulfilment/preparing-the-shipment#1500" target="_blank">table of shipping charges</a> for **Spring GDS**.

### Register orders to Spring GDS in the Shipping Centre
Orders can be registered to Spring GDS using the **Orders » Shipping centre**.
  * In the **Search** tab search and select the orders you want to ship with Spring GDS. 
  * In the **Register** tab select **Shipping service provider** - **Spring GDS** and click the **Register** icon.
  * This will generate a Spring shipping label for the orders and set the **Package number** with the tracking link.
