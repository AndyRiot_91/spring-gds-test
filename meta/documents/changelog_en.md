# Release Notes for Spring GDS Versand

## v1.0.2 (2020-02-21)
 
 ### Added
- Option: Reference on label
 
### Fixed
- Product value error for non-EU destinations

## v1.0.1 (2020-02-06)
 
### Fixed
- User guide
 
## v1.0.0 (2020-01-24)
 
### Added
- Initial plugin version